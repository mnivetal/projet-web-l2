# Projet Web L2
## Table des contenus:
### Description
### Statut du projet
### Frameworks utilisés
### Instruction d'installation
### Bugs connus - Corrections apportées 
### Droit d'auteurs


## Description :

Projet web en Symfony et vueJs. Il s'agit d'un site où l'on peut retrouver des recettes de cuisine, il y'a une partie utilisateur et administrateur. Pour la partie Symfony, on peut se login en tant qu'admin ou en tant qu'utilisateur ayant un profil et pouvant réserver des cours de cuisine. La partie vueJs permet la recherche de recettes de cuisine grâce à une barre de recherche. 



## Statut du projet:

Le projet est terminé depuis juin 2021 cependant des améliorations et des mises à jour peuvent être effectuées afin d'améliorer le site.

## Frameworks utilisés
Le projet a été conçu à partir des frameworks symfony et vueJs sur les IDE PhpStorm et WebStorm.

Version de PhpStorm:
PhpStorm 2020.2.3
Runtime version: 11.0.8+10-b944.34 x86_64
Version de WebStorm:
WebStorm 2020.3.3
Runtime version: 11.0.10+8-b1145.96 x86_64



## Instruction d'installation
Installation des dépendances liés au projet Symfony:

Composer:
    composer req api 
	composer require --dev profiler maker
	composer require annotations twig form validator orm asset
	composer require beberlei/doctrineextensions(pour utiliser des fonctions SQL)
	composer require vich/uploader-bundle //pour l’upload de fichier
	composer require symfony/maker-bundle —dev (que j’ai utiliser pour faire mon formulaire 		de création de compte)
	bundle API Platform
Doctrine:
	php bin/console doctrine:database:create
	php bin/console doctrine:schema:update
	php bin/console doctrine:schema:update —force
	composer require twig/extensions

Dans le services.yalm :

Celle-ci m’a permis d’utiliser les fonctions sql pour doctrine
doctrine:
    orm:
        dql:
            numeric_functions:
                rand: DoctrineExtensions\Query\Mysql\Rand



Installation des dépendances liés au projet vueJs:

$ npm install vue

"dependencies": {

    "axios": "^0.21.1",

    "core-js": "^3.6.5",

    "vue": "^2.6.11",

    "vue-router": "^3.2.0",

    "vuex": "^3.4.0"
  },


  "devDependencies": {
    "@vue/cli-plugin-babel": "~4.5.0",

    "@vue/cli-plugin-eslint": "~4.5.0",

    "@vue/cli-plugin-router": "~4.5.0",

    "@vue/cli-plugin-vuex": "~4.5.0",

    "@vue/cli-service": "~4.5.0",

    "babel-eslint": "^10.1.0",

    "eslint": "^6.7.2",

    "eslint-plugin-vue": "^6.2.2",
    
    "vue-template-compiler": "^2.6.11"
  }
## Bugs connus / corrections apportées

bug du 10/03/2021
Pour la partie vueJs, la page de détail des recettes n'affichait pas la recette, après modification de la fonction filteredRecette le bug a été corrigé et le détail de la recette s'affiche correctement.

## Droits d'auteurs: 

Site réalisé dans son intégrité qu'il s'agisse de la partie backend ou frontend par Marie Nivet.
HTML / CSS réalisé par Marie Nivet.


